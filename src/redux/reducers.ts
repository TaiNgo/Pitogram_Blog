import { combineReducers } from "redux";

import counter from "@redux/slices/counter";
import posts from "@redux/slices/posts";

const rootReducer = combineReducers({ counter, posts });

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
