import { createSlice } from "@reduxjs/toolkit";

const initialState: any = {
  list: [],
};

const counterSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setPosts: (state, { payload: data }) => {
      state.list = [...data];
    },
  },
});

export const { setPosts } = counterSlice.actions;

export default counterSlice.reducer;
