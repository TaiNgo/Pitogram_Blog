import dynamic from "next/dynamic";
import React from "react";
import hljs from 'highlight.js';
import _ from 'lodash'

const QuillNoSSRWrapper = dynamic(import("react-quill"), {
  ssr: false,
  loading: () => <p></p>,
});

hljs.configure({
  languages: ['javascript', 'ruby', 'python', 'rust'],
});

function MyEditor({ data, callBack = () => {}, readOnly }) {
  const modules = {
    syntax: {
      highlight: text => hljs.highlightAuto(text).value,
    },
    toolbar: [
      [{ header: "1" }, { header: "2" }],
      [{ size: [] }],
      ["bold", "italic", "underline", "strike", "blockquote"],
      [
        { list: "ordered" },
        { list: "bullet" },
        { indent: "-1" },
        { indent: "+1" },
      ],
      ["link", "image", "video"],
      ["clean"],
      ['code-block']
    ],
    clipboard: {
      // toggle to add extra line breaks when pasting HTML:
      matchVisual: false,
    }
  };
  /*
   * Quill editor formats
   * See https://quilljs.com/docs/formats/
   */
  const formats = [
    "header",
    "size",
    "bold",
    "italic",
    "underline",
    "strike",
    "blockquote",
    "list",
    "bullet",
    "indent",
    "link",
    "image",
    "video",
    "code-block"
  ];

  return <QuillNoSSRWrapper  modules={modules} formats={formats} theme={readOnly ? "bubble" : "snow"} defaultValue={data} onChange={_.debounce(callBack)} readOnly={readOnly}/>;
}

export default MyEditor;
