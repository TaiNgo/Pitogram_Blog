import React, { useState } from "react";
import style from "./index.module.scss";
import Link from "next/link";
import { useMediaQuery } from "react-responsive";
import { Space, Button, Menu, Drawer, Affix, Dropdown } from "antd";
import { toolList } from "@shared/toolListData";

export const Header = () => {
  const [visible, setVisible] = useState(false);
  const isTabletOrMobile = useMediaQuery({ query: "(max-width: 822px)" });

  const showDrawer = () => {
    setVisible(true);
  };
  const onClose = () => {
    setVisible(false);
  };

  const burgurIcon = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-list"
      viewBox="0 0 16 16"
    >
      <path
        fillRule="evenodd"
        d="M2.5 12a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h10a.5.5 0 0 1 0 1H3a.5.5 0 0 1-.5-.5z"
      />
    </svg>
  );

  const toolMenu = (
    <Menu>
      {Object.keys(toolList).map((key) => {
        return (
          <Menu.Item key={key} icon={toolList[key]["icon"]}>
            <Link href={`/tools/${key}`}>{toolList[key]["title"]}</Link>
          </Menu.Item>
        );
      })}

      {/* hard code this */}
      {/* <Menu.Item
        key="datecounter"
        icon={
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="16"
            height="16"
            fill="currentColor"
            className="bi bi-calendar-date"
            viewBox="0 0 16 16"
          >
            <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
          </svg>
        }
      >
        <Link href={`/d`}>Date Counter</Link>
      </Menu.Item> */}
    </Menu>
  );

  const navMenu = (
    <>
      <Button type="text" onClick={showDrawer} size="large">
        {burgurIcon}
      </Button>
      <Drawer
        placement="right"
        closable={false}
        onClose={onClose}
        visible={visible}
        style={{ paddingRight: 0 }}
      >
        <Menu mode="inline" onClick={() => setVisible(false)}>
          {Object.keys(toolList).map((key) => {
            return (
              <Menu.Item key={key} icon={toolList[key]["icon"]}>
                <Link href={`/tools/${key}`}>{toolList[key]["title"]}</Link>
              </Menu.Item>
            );
          })}
          {/* hard code this */}
          <Menu.Item
            key="datecounter"
            icon={
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-calendar-date"
                viewBox="0 0 16 16"
              >
                <path d="M6.445 11.688V6.354h-.633A12.6 12.6 0 0 0 4.5 7.16v.695c.375-.257.969-.62 1.258-.777h.012v4.61h.675zm1.188-1.305c.047.64.594 1.406 1.703 1.406 1.258 0 2-1.066 2-2.871 0-1.934-.781-2.668-1.953-2.668-.926 0-1.797.672-1.797 1.809 0 1.16.824 1.77 1.676 1.77.746 0 1.23-.376 1.383-.79h.027c-.004 1.316-.461 2.164-1.305 2.164-.664 0-1.008-.45-1.05-.82h-.684zm2.953-2.317c0 .696-.559 1.18-1.184 1.18-.601 0-1.144-.383-1.144-1.2 0-.823.582-1.21 1.168-1.21.633 0 1.16.398 1.16 1.23z" />
                <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5zM1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4H1z" />
              </svg>
            }
          >
            <Link href={`/d`}>Date Counter</Link>
          </Menu.Item>
        </Menu>
      </Drawer>
    </>
  );

  return (
    <Affix offsetTop={0}>
      <div
        style={{ backgroundColor: "#20232a", textAlign: "left" }}
        className={style.header}
      >
        <div className={style.container}>
          <Link href="/" replace>
            <img className={`${style.logo}`} src="/images/logo.png" />
          </Link>
          <Space size={isTabletOrMobile ? 0 : 48} className={style.navigations}>
            {isTabletOrMobile ? (
              navMenu
            ) : (
              <>
                <Dropdown
                  // @ts-ignore
                  overlay={toolMenu}
                  trigger={["click"]}
                >
                  <div
                    className="ant-dropdown-link"
                    onClick={(e) => e.preventDefault()}
                  >
                    <a className={style.navigate_button}>Tools</a>
                  </div>
                </Dropdown>
              </>
            )}
          </Space>
        </div>
      </div>
    </Affix>
  );
};
