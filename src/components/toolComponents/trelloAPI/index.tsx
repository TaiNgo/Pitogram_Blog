import React, { useEffect, useState } from "react";
import _ from "lodash";
import style from "./index.module.scss";
import { Button, Divider, Input, message, Popconfirm, Spin } from "antd";
import axios from "axios";
import { GetAllDownloadTask, ModifyDownloadTask, DeleteTask } from "@firebaseSDK/firebaseAPI/tools/TrelloManager";

//trello
const APIKey = "2efe513b6ca7079fbfc916dd7a3e9a11";

export const TrelloAPI = () => {
  const [userToken, setUserToken] = useState("");
  const [isValidToken, setIsValidToken] = useState("");
  const [boardList, setBoardList] = useState([]);
  const [listInBoard, setListInBoard] = useState([]);
  const [currentBoard, setCurrentBoard] = useState({});
  const [downloadTasks, setDownloadTasks] = useState([] as any);

  const [logingin, setLogingin] = useState(false);

  const error = (text) => {
    console.log(text);
    message.error({
      content: text,
      style: {
        marginTop: "10vh",
      },
    });
  };

  const success = (text) => {
    message.success({
      content: text,
      style: {
        marginTop: "20vh",
      },
    });
  };

  useEffect(() => {
    //try to login
    let token = localStorage.getItem("token");
    token && getUser(localStorage.getItem("token"));

    //cron fetch task status
    GetAllDownloadTask().then((res: any) => {
      setDownloadTasks(res["data"]);
    });

    setInterval(() => {
      GetAllDownloadTask().then((res: any) => {
        setDownloadTasks(res["data"]);
      });
    }, 5000);
  }, []);

  useEffect(() => {
    if (userToken)
      if (isValidToken == "true") {
        success("Logged in!");
        localStorage.setItem("token", userToken);
        getBoards();
      }
    if (isValidToken == "false") {
      error("Invalid token!");
    }
  }, [isValidToken]);

  const icon = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
      fill="currentColor"
      className="bi bi-textarea-t"
      viewBox="0 0 16 16"
    >
      <path d="M1.5 2.5A1.5 1.5 0 0 1 3 1h10a1.5 1.5 0 0 1 1.5 1.5v3.563a2 2 0 0 1 0 3.874V13.5A1.5 1.5 0 0 1 13 15H3a1.5 1.5 0 0 1-1.5-1.5V9.937a2 2 0 0 1 0-3.874V2.5zm1 3.563a2 2 0 0 1 0 3.874V13.5a.5.5 0 0 0 .5.5h10a.5.5 0 0 0 .5-.5V9.937a2 2 0 0 1 0-3.874V2.5A.5.5 0 0 0 13 2H3a.5.5 0 0 0-.5.5v3.563zM2 7a1 1 0 1 0 0 2 1 1 0 0 0 0-2zm12 0a1 1 0 1 0 0 2 1 1 0 0 0 0-2z" />
      <path d="M11.434 4H4.566L4.5 5.994h.386c.21-1.252.612-1.446 2.173-1.495l.343-.011v6.343c0 .537-.116.665-1.049.748V12h3.294v-.421c-.938-.083-1.054-.21-1.054-.748V4.488l.348.01c1.56.05 1.963.244 2.173 1.496h.386L11.434 4z" />
    </svg>
  );

  const getBoards = () => {
    setBoardList([]);
    axios.get(`/api/trello/boards?userToken=${userToken}`).then((res) => {
      if (!res["data"]["success"]) {
        error("Cannot get boards!");
      } else {
        setBoardList(res["data"]["data"]);
      }
    });
  };

  const getList = ({ id, name }) => {
    setCurrentBoard({
      id,
      name,
    });
    setListInBoard([]);
    axios
      .get(`/api/trello/list?userToken=${userToken}&boardId=${id}`)
      .then((res) => {
        if (!res["data"]["success"]) {
          error("Cannot get list!");
        } else {
          setListInBoard(res["data"]["data"]);
        }
      });
  };

  //try to get user info with the token
  const getUser = (token) => {
    setLogingin(true);
    if (token.length == 0) {
      error("Token is empty!");
      setLogingin(false);
    } else {
      axios.get(`/api/trello/userInfo?userToken=${token}`).then((res) => {
        if (!res["data"]["success"]) {
          setIsValidToken(() => "false");
        } else {
          setUserToken(token);
          setIsValidToken(() => "true");
        }
        setLogingin(false);
      });
    }
  };

  const downloadAttachments = async (listId) => {
    let newTasks = [
      ...downloadTasks.filter((t) => t["id"] != listId),
      { id: listId, status: 0 },
    ];
    setDownloadTasks(newTasks);

    //init empty download task in DB
    ModifyDownloadTask(listId, "", 0, 0, "");

    axios
      .get(`/api/trello/cards?userToken=${userToken}&listId=${listId}`, {})
      .then(async (res) => {
        if (!res["data"]["success"]) {
          error("Cannot get cards!");
        } else {
          let imgData = [] as any;
          let cardList = res["data"]["data"];

          let listAttachment = await Promise.all(
            cardList.map((c) => {
              return getAttachments(c["id"]);
            })
          );

          //map image urls by card name
          for (let i = 0; i < cardList.length; i++) {
            const c = cardList[i];
            let name = c["name"];
            let urls: any = listAttachment[i];
            if (urls.length > 0) {
              imgData.push({
                [name]: urls,
              });
            }
          }

          let filesCount = imgData
            .map((v) => v[Object.keys(v)[0]])
            .flat().length;
          let currList = listInBoard.find((v) => v["id"] == listId) || {};
          let folderName =
            currList && `${currentBoard["name"]} - ${currList["name"]}`;

          if (imgData.length == 0) {
            //delete task if there is no data to download
            //listId is taskId as well
            DeleteTask(listId).then(() => {
              //refresh the download status list
              GetAllDownloadTask().then((res: any) => {
                setDownloadTasks(res["data"]);
              });
            });
            error("No any data!");
          } else {
            //update download task with folder name and filesCount
            ModifyDownloadTask(listId, folderName, filesCount, 0, "").then(
              () => {
                axios
                  .post(
                    `/api/trello/downloadAttachments`,
                    {
                      folderName,
                      imgData,
                      downloadTaskId: listId,
                    },
                    {
                      headers: {
                        APIKey,
                        userToken,
                      },
                    }
                  )
                  .then((res) => {
                    //refresh the download status list
                    GetAllDownloadTask().then((res: any) => {
                      setDownloadTasks(res["data"]);
                    });

                    if (!res["data"]["success"]) {
                      error(res["data"]["error"]);
                    } else {
                      success(
                        `Created download task for ${currentBoard["name"]} - ${currList["name"]}!`
                      );
                    }
                  });
              }
            );
          }
        }
      });
  };

  const getAttachments = (cardId) => {
    return new Promise((resolve, reject) => {
      axios
        .get(`/api/trello/attachment?userToken=${userToken}&cardId=${cardId}`)
        .then((res) => {
          if (!res["data"]["success"]) {
            resolve("");
          } else {
            resolve(
              res["data"]["data"].map((att) => {
                return att["url"];
              })
            );
          }
        });
    });
  };

  const hasDownloadingTask = (listId) => {
    return downloadTasks.filter((t) => t["id"] == listId).length != 0;
  };

  const getTaskByListId = (listId) => {
    let task = downloadTasks.find((t) => t["id"] == listId);
    return task || {};
  };

  const renderActionButtons = (listId) => {
    if (hasDownloadingTask(listId)) {
      let task = getTaskByListId(listId);
      if (task["status"] == 0) {
        return <p className={`${style.gettingData}`}>Getting data...</p>;
      }
      if (task["status"] == 1) {
        return (
          <>
            <Button type="link" onClick={() => downloadAttachments(listId)}>
              Refresh
            </Button>
            <Button type="primary">
              <a href={task["url"]} target="_blank">
                Download
              </a>
            </Button>
          </>
        );
      }
    } else {
      return (
        <Button type="dashed" onClick={() => downloadAttachments(listId)}>
          Get data
        </Button>
      );
    }
  };

  const renderTaskStatus = (listId) => {
    let filesCount = getTaskByListId(listId)["filesCount"];
    let hasDownloadTask = hasDownloadingTask(listId);
    if (hasDownloadTask) {
      if (!filesCount || filesCount == 0) {
        return <span>...</span>;
      } else {
        return (
          <>
            <span>Got</span>
            <span style={{ color: "red" }}>{filesCount}</span>
            <span>files!</span>
          </>
        );
      }
    }
  };

  const logout = () => {
    window.localStorage.removeItem("token");
    setUserToken("");
    setIsValidToken("");
    setBoardList([]);
    setListInBoard([]);
  };

  return (
    <Spin spinning={logingin}>
      {/* code tool o day */}
      {isValidToken != "true" ? (
        <div className={`${style.loginSection}`}>
          <Input
            placeholder="Enter your token here!"
            onChange={(e) => setUserToken(e.target.value)}
          />
          <Button
            type="primary"
            size="large"
            onClick={() => getUser(userToken)}
          >
            Login
          </Button>

          <Button type="link">
            <a
              target="_blank"
              href={`https://trello.com/1/connect?key=${APIKey}&name=MyApp&response_type=token&expiration=never&scope=read,write`}
            >
              Get Token
            </a>
          </Button>
        </div>
      ) : (
        <div className={`${style.loginSection}`}>
          <Popconfirm
            title="Are you sure to logout?"
            onConfirm={logout}
            okText="Yes"
            cancelText="No"
          >
            <Button type="dashed">Log out</Button>
          </Popconfirm>
        </div>
      )}

      <div className={`${style.data}`}>
        <h2>Boards</h2>
        <div className={`${style.boards}`}>
          {boardList.length == 0 && <p>...</p>}
          {boardList.map((b) => (
            <Button
              key={b["id"]}
              size="large"
              type={currentBoard["id"] == b["id"] ? "primary" : "default"}
              onClick={() => getList(b)}
            >
              {b["name"]}
            </Button>
          ))}
        </div>
        <h2>List</h2>
        <div className={`${style.list}`}>
          {listInBoard.length == 0 && <p>...</p>}
          {listInBoard.map((l) => (
            <div key={l["id"]} className={`${style.item}`}>
              <p style={{ fontSize: "large" }}>{l["name"]}</p>
              <div className={`${style.task}`}>
                <div className={`${style.count}`}>
                  {renderTaskStatus(l["id"])}
                </div>
                <Divider type="vertical" />
                <div className={`${style.actionButtons}`}>
                  {renderActionButtons(l["id"])}
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </Spin>
  );
};
