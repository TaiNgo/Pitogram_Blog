import React, { useEffect, useState } from "react";
import _ from "lodash";

import style from "./index.module.scss";
import { Button, Checkbox, Input, message, Tag } from "antd";
import * as shortId from "shortid";
import {
  CreateShortLink,
  GetAllLink,
} from "@firebaseSDK/firebaseAPI/tools/ShortLinkManager";

export const ShortLink = () => {
  const [id, setId] = useState("");
  const [url, setUrl] = useState("");
  const [creating, setCreating] = useState(false);
  const [shortedLink, setShortedLink] = useState("");
  const [isCustomID, setIsCustomID] = useState(false);
  const [urls, setUrls] = useState([]);


  const error = (text) => {
    message.error(text);
  };

  useEffect(() => {
    GetAllLink().then((res: any) => {
      setUrls(res["data"]);
    });
  }, []);

  function isValidHttpUrl(string) {
    let url;

    try {
      url = new URL(string);
    } catch (_) {
      return false;
    }

    return url.protocol === "http:" || url.protocol === "https:";
  }

  const create = async () => {
    if (url.length == 0 || !isValidHttpUrl(url)) {
      error("Your URL is not correct!");
    } else {
      setCreating(true);
      setShortedLink("");
      let newId = id;
      if (id.length == 0) {
        newId = shortId.generate();
      }
      let res: any = await CreateShortLink(url, newId);

      if (res["success"]) {
        setShortedLink(res["data"]["shortedLink"]);
        setCreating(false);
        GetAllLink().then((res: any) => {
          setUrls(res["data"]);
        });
      } else {
        setCreating(false);
        error(res["error"]);
      }
    }

  };

  useEffect(() => {
    setId("");
  }, [isCustomID]);

  return (
    <>
      {/* code tool o day */}
      <div className={`${style.creating_section}`}>
        <h3>Source</h3>
        <div className={`${style.short_link}`}>
          <Input onChange={_.debounce((e) => setUrl(e.target.value), 100)} />
        </div>
        {isCustomID && (
          <>
            <h3>Short link</h3>
            <div className={`${style.short_link}`}>
              <p>pitogram.com/l/</p>
              <Input
                placeholder="your ID"
                onChange={_.debounce((e) => setId(e.target.value), 100)}
              />
            </div>
          </>
        )}
        <Checkbox onChange={(e) => setIsCustomID(e.target.checked)}>
          Custom ID?
        </Checkbox>
        <Button
          onClick={() => create()}
          className={`${style.create_link}`}
          type="primary"
          loading={creating}
        >
          Create
        </Button>
      </div>

      <div className={`${style.container}`}>
        <div className={`${style.result}`}>
          <h3>Your link</h3>
          <a href={shortedLink} target="_blank">
            {shortedLink.split("//")[1] || "..."}
          </a>
        </div>
      </div>

      {/* <div className={`${style.container}`}>
        <div className={`${style.result}`}>
          <h3>Recent created</h3>
          <div className={`${style.links}`}>
            {urls.map((l: any) => {
              return (
                <>
                  <Tag className={`${style.tag}`}>
                    <a href={l.shortedLink} target="_blank">
                      {l.shortedLink.split("//")[1].replaceAll("localhost:3000", "pitogram.com") || "..."}
                    </a>
                  </Tag>
                </>
              );
            })}
          </div>
        </div>
      </div> */}
    </>
  );
};
