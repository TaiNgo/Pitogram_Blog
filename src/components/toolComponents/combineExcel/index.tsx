import React, { useState } from "react";
import _ from "lodash";
import { Button, Upload, message, Input } from "antd";
import { InboxOutlined } from "@ant-design/icons";
const { Dragger } = Upload;
import style from "./index.module.scss";

//xsl
import * as XLSX from "xlsx";
import axios from "axios";

export const CombineExcel = () => {
  const [fileList, setFileList] = useState([] as any[]);
  const [uploading, setUploading] = useState(false);

  const error = (text) => {
    message.error(text);
  };

  const handleChange = (info) => {
    const { status } = info.file;
    console.log(status);
    setUploading(status == "uploading");

    let fileList = [...info.fileList];

    // 1. Limit the number of uploaded files
    // Only to show two recent uploaded files, and old ones will be replaced by the new
    fileList = fileList.slice(-100);

    //remove all non-excel files
    fileList = fileList.filter((v) => v.name.indexOf(".xlsx") !== -1);

    // 2. Read from response and show file link
    fileList = fileList.map((file) => {
      if (file.response) {
        // Component will show file.url as link
        file.url = file.response.url;
      }
      return file;
    });

    setFileList(fileList);
  };

  const props = {
    name: "file",
    multiple: true,
    accept: ".xlsx",
    onChange: handleChange,
    fileList,
    customRequest: (options) => {
      const { onSuccess } = options;
      axios.get('https://reqres.in/api/users').then((data) => onSuccess(data)).catch((err) => onSuccess(err))
    },
  };

  const combine = async () => {
    let combinedWorkBook: XLSX.WorkBook = null as any;

    for (let i = 0; i < fileList.length; i++) {
      const element = fileList[i];
      const data = await element.originFileObj.arrayBuffer();
      let workbook = XLSX.readFile(data);
      if (!combinedWorkBook) {
        combinedWorkBook = workbook; //init data
      } else {
        let currentSheet = workbook.Sheets[Object.keys(workbook.Sheets)[0]]; //get the first sheet
        let combinedSheet =
          combinedWorkBook.Sheets[Object.keys(combinedWorkBook.Sheets)[0]];
        let aoa = XLSX.utils
          .sheet_to_json(currentSheet)
          .map((v: Object) => [...Object.values(v)]);
        XLSX.utils.sheet_add_aoa(combinedSheet, aoa, { origin: -1 });
      }
    }
    XLSX.writeFile(combinedWorkBook, "Combined.xlsx", {
      type: "file",
    });
  };

  return (
    <div className={`${style.main}`}>
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">
          Click or drag file to this area to upload
        </p>
        <p className="ant-upload-hint">
          Support for a single or bulk upload. Strictly prohibit from uploading
          company data or other band files
        </p>
      </Dragger>
      {fileList.length > 0 && !uploading && (
        <Button
          className={`${style.clear_files}`}
          onClick={() => setFileList(() => [])}
        >
          Clear all
        </Button>
      )}
      <Button
        type="primary"
        size="large"
        disabled={uploading || fileList.length == 0}
        className={`${style.combine_button}`}
        onClick={() => combine()}
      >
        Combine
      </Button>
    </div>
  );
};
