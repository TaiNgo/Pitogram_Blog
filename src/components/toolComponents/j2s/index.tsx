import React, { useEffect, useState } from "react";
import _ from "lodash";
import { Button, Checkbox, message, Input } from "antd";
import style from "./index.module.scss";

const { TextArea } = Input;

const defaultString = `[    {        \"name\": \"ad 2/12\",        \"callToAction\": \"LEARN_MORE\",        \"imageIds\": [              \"v0201/ac1362a198f54b8cbc31575dcd5597d0\"        ],        \"adFormat\": \"SINGLE_VIDEO\",        \"adText\": \"lorem\",        \"videoId\": \"v10033g50000c41enrrc77ud6c227lug\",        \"displayName\": \"Testad\",        \"openUrlType\": \"NORMAL\",        \"fallbackType\": \"UNSET\",        \"isNewStructure\": true,        \"landingPageUrl\": \"https://knorex.com\"    },    {        \"name\": \"ad2/122\",        \"callToAction\": \"LEARN_MORE\",        \"imageIds\": [              \"v0201/ac1362a198f54b8cbc31575dcd5597d0\"        ],        \"adFormat\": \"SINGLE_VIDEO\",        \"adText\": \"lorem\",        \"videoId\": \"v10033g50000c41enrrc77ud6c227lug\",        \"displayName\": \"Testad\",        \"openUrlType\": \"NORMAL\",        \"fallbackType\": \"UNSET\",        \"isNewStructure\": true,        \"landingPageUrl\": \"https://knorex.com\"    }]`;

const defaultJson = `[
    {
        "name": "ad 2/12",
        "callToAction": "LEARN_MORE",
        "imageIds": [
              "v0201/ac1362a198f54b8cbc31575dcd5597d0"
        ],
        "adFormat": "SINGLE_VIDEO",
        "adText": "lorem",
        "videoId": "v10033g50000c41enrrc77ud6c227lug",
        "displayName": "Testad",
        "openUrlType": "NORMAL",
        "fallbackType": "UNSET",
        "isNewStructure": true,
        "landingPageUrl": "https://knorex.com"
    },
    {
        "name": "ad2/122",
        "callToAction": "LEARN_MORE",
        "imageIds": [
              "v0201/ac1362a198f54b8cbc31575dcd5597d0"
        ],
        "adFormat": "SINGLE_VIDEO",
        "adText": "lorem",
        "videoId": "v10033g50000c41enrrc77ud6c227lug",
        "displayName": "Testad",
        "openUrlType": "NORMAL",
        "fallbackType": "UNSET",
        "isNewStructure": true,
        "landingPageUrl": "https://knorex.com"
    }
]`;

export const JsonToString = () => {
  const [string, setString] = useState(defaultString);
  const [json, setJson] = useState(defaultJson);
  const [outputString, setOutputString] = useState("");
  const [outputJson, setOutputJson] = useState("");
  const [multiLine, setMultiLine] = useState(false);

  const error = (text) => {
    message.error(text);
  };

  useEffect(() => {
    Json2String();
  }, [multiLine]);

  const Json2String = () => {
    setOutputString("");
    multiLine
      ? setOutputString(JSON.stringify(json).slice(1, -1))
      : setOutputString(
          JSON.stringify(json)
            .replace(/\\n/g, "")
            .slice(1, -1)
        );
  };

  const string2Json = () => {
    setOutputJson("");
    try {
      if (string.length > 0) {
        let result = string
          .replaceAll("\\n", "")
          .replaceAll("\\t", "")
          .replaceAll("\\", "")
          .trim();

        result = JSON.parse(result);
        setOutputJson(JSON.stringify(result, null, 5));
      } else setOutputJson("Nothing!");
    } catch (e) {
      setOutputJson("Something went wrong!");
    }
  };

  return (
    <div className={`${style.main}`}>
      <div className={`${style.convert_section}`}>
        <div className={`${style.input}`}>
          <label>JSON</label>
          <TextArea
            rows={15}
            allowClear
            autoSize={{ minRows: 15, maxRows: 15 }}
            onChange={(e) => setJson(e.target.value)}
            defaultValue={defaultJson}
          />
        </div>
        <div className={`${style.options}`}>
          <Button
            type="primary"
            className={`${style.convert_button}`}
            onClick={() => Json2String()}
          >
            {`to String =>`}
          </Button>
          <Checkbox
            className={`${style.checkbox}`}
            onChange={(e) => setMultiLine(e.target.checked)}
          >
            Multi line?
          </Checkbox>
        </div>
        <div className={`${style.output}`}>
          <label>String</label>
          <TextArea
            rows={15}
            autoSize={{ minRows: 15, maxRows: 15 }}
            value={outputString}
          />
        </div>
      </div>

      {/* String to JSON */}
      <div className={`${style.convert_section}`}>
        <div className={`${style.input}`}>
          <label>String</label>
          <TextArea
            rows={15}
            allowClear
            autoSize={{ minRows: 15, maxRows: 15 }}
            onChange={(e) => setString(e.target.value)}
            defaultValue={defaultString}
          />
        </div>
        <div className={`${style.options}`}>
          <Button
            type="primary"
            className={`${style.convert_button}`}
            onClick={() => string2Json()}
          >
            {`to JSON =>`}
          </Button>
        </div>
        <div className={`${style.output}`}>
          <label>JSON</label>
          <TextArea
            rows={15}
            autoSize={{ minRows: 15, maxRows: 15 }}
            value={outputJson?.toString()}
          />
        </div>
      </div>
    </div>
  );
};
