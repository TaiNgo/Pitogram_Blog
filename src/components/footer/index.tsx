import React, { CSSProperties } from "react";
import Image from "next/image";
import { Space } from "antd";
import {
  GithubOutlined,
  TwitterOutlined,
  YoutubeOutlined,
  LinkedinOutlined,
  FacebookFilled,
} from "@ant-design/icons";

export const Footer: React.FC = () => {
  const iconStyle: CSSProperties = {
    fontSize: 22,
    color: "#fff",
  };
  return (
    <div
      style={{
        backgroundColor: "#282c34",
        color: "#fff",
        textAlign: "center",
        paddingTop: 32,
        paddingBottom: 32,
      }}
    >
      <Space direction="vertical" size="large">
        <Space align="center" size="middle">
          <a
            href="https://fb.com/tai.ngooo"
            target="_blank"
            style={iconStyle}
          >
            <FacebookFilled />
          </a>
        </Space>
      </Space>
    </div>
  );
};
