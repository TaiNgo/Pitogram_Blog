import React from "react";
import style from "./index.module.scss";
import Link from "next/link";
import { Avatar, Tag, Tooltip } from "antd";

const PostCard = ({ id, imgSrc, title, authorName, time, avtSrc, content }) => {
  const renderTags = () => {
    let regex = /(?<=\s|^)#(\w*[A-Za-z_]+\w*)/g;
    let hashTags = content.match(regex);
    let colors = {
      "#nsfw": "red",
    };
    return hashTags?.slice(0, 2).map((h, k) => {
      return (
        <Tag key={k} color={colors[h] ? colors[h] : "blue"}>
          {h}
        </Tag>
      );
    });
  };

  return (
    <Link href={`/${id}`}>
      <a className={`${style.card}`} style={{ color: "black" }}>
        <img src={imgSrc} className={`${style.image}`} />
        <div className={`${style.info}`}>
          <p className={style.title}>{title}</p>
          <div className={`${style.tags}`}>{renderTags()}</div>
          <div className={style.card_author}>
            <Avatar size={32} src={avtSrc} />
            <div className={`${style.name_time}`}>
              <p className={style.name}>{authorName}</p>
              <p className={style.time}>{time}</p>
            </div>
          </div>
        </div>
      </a>
    </Link>
  );
};

export default PostCard;
