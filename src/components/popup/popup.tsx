import React, { useState } from "react";
import style from "./index.module.scss";
import { Popconfirm, message } from "antd";

function cancel(e) {
  
}

const Popup = ({ record, visible, hidePopup, x, y, callBack }) => {

  return visible && (
    <ul className={style.popup} style={{ left: `${x}px`, top: `${y}px` }}>
      <li onClick={() => callBack("edit", record)}>
        <img src="./icons/edit.svg" />
        Edit
      </li>
      <Popconfirm
        title="Chắc khum?"
        onConfirm={() => callBack("delete", record)}
        onCancel={() => hidePopup()}
        okText="Có"
        cancelText="Thôi"
        placement="bottom"
      >
        <li>
          <img src="./icons/delete.svg" />
          Delete
        </li>
      </Popconfirm>
    </ul>
  );
  }
export default Popup;
