import { PostList } from "@components";
import React from "react";
import style from "./index.module.scss";

export const Main = () => {
  return (
    <div
      style={{
        textAlign: "center",
        paddingBottom: 32,
      }}
      className={style.main}
    >
      <img className={`${style.logo}`} src="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/18167163.png?alt=media&token=27214071-bfcd-4a73-91a9-963d94a00bfe" />

      <PostList title="New posts" />
    </div>
  );
};
