import Link from "next/link";
import React from "react";
import style from "./index.module.scss";

const AppCard = ({ imgSrc, title, preText, link }) => {

  return (
    <Link href={link}>
      <div className={`${style.card}`}>
        <img
          src={imgSrc}
          alt="Picture of the author"
          className={`${style.image}`}
        />
        <p className={style.title}>{title}</p>
        <p className={style.pre_text}>{preText}</p>
      </div>
    </Link>
  );
};

export default AppCard;
