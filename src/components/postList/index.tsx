import PostCard from "@components/postCard";
import { Post } from "@types";
import React, { useEffect, useState } from "react";
//dayjs
import dayjs from "dayjs";
import "dayjs/locale/en";
dayjs.locale("en");

import style from "./index.module.scss";
import { RootState } from "@redux/reducers";
import { useSelector } from "react-redux";

export const PostList = ({ title }) => {
  const posts = useSelector((state: RootState) => state.posts.list);

  const parseTime = (time) => {
    return dayjs(time).format("DD/MM/YYYY, [at] HH:mm");
  };

  const renderPosts = (): any => {
    return posts.map((v: Post, k) => {
      return (
        <PostCard
          key={v.id}
          id={v.id}
          imgSrc={v.thumbnailUrl}
          title={v.title}
          authorName={v.authorID}
          time={parseTime(v.created)}
          content={v.content}
          avtSrc={
            "https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/0019_17A.jpg?alt=media&token=49024a1b-23d2-48f2-b18d-8ba61ca1c1e2"
          }
        />
      );
    });
  };

  return (
    <>
      <div className={style.post_list}>
        <p className={style.title}>{title}</p>
        <br></br>
        <div className={style.cards}>{renderPosts()}</div>
      </div>
    </>
  );
};
