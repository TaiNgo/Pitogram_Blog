const generateErrorResponse = (error) => {
  return {
    success: false,
    error,
  };
};

const generateSuccessResponse = (data) => {
  return {
    success: true,
    data,
  };
};

export { generateErrorResponse, generateSuccessResponse };
