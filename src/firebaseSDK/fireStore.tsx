import { fireStore, firebase } from "./firebase";
import dayjs from "dayjs";
import * as shortId from "shortid";

//==========================TOOLS============================================

const getSpecialDates = () => {
  let ref = fireStore.collection("special_dates");
  return ref.get();
};

const deleteTask = (id) => {
  let ref = fireStore.collection("trello_download_tasks").doc(id);
  return ref.delete();
};

const getAllTrelloTasks = () => {
  let ref = fireStore.collection("trello_download_tasks");
  return ref.get();
};

const downloadTaskTrello = (id, fileName, filesCount, status, url) => {
  // Add a new document with a generated id.
  let ref = fireStore.collection("trello_download_tasks").doc(id);

  //0: downloading 1: downloaded
  let data = {
    fileName,
    filesCount,
    status,
    url,
  };

  return ref.set(data);
};

const getAllShortLink = () => {
  let ref = fireStore.collection("shortLinks").orderBy("shortedLink");
  return ref.get();
};

const getShortLink = (id) => {
  let ref = fireStore.collection("shortLinks").doc(id);
  return ref.get();
};

const createShortLink = (url, id) => {
  // Add a new document with a generated id.
  let newPostRef = fireStore.collection("shortLinks").doc(id);
  // later...
  let urlNew = new URL(url);

  let data = {
    shortedLink: `${process.env.NEXT_PUBLIC_DOMAIN}/l/${id}`,
    source: urlNew.toString(),
  };
  return newPostRef.set(data);
};

//==========================POST============================================
const createPost = (data) => {
  // Add a new document with a generated id.
  let newPostRef = fireStore.collection("posts").doc(shortId.generate());
  // later...
  return newPostRef.set(data);
};

const updatePost = (data) => {
  let { id, ...values } = data;
  // Add a new document with a generated id.
  let newPostRef = fireStore.collection("posts").doc(id);
  // later...
  return newPostRef.update(values);
};

const updateView = (postId) => {
  let ref = fireStore.collection("posts").doc(postId);
  return ref.update({
    views: firebase.firestore.FieldValue.increment(1),
  });
};

const deletePost = (id) => {
  return fireStore.collection("posts").doc(id).delete();
};

const getAllPosts = () => {
  let ref = fireStore.collection("posts").orderBy("lastModified", "desc");
  return ref.get();
};

const getRandomPosts = (howMany) => {
  let ref = fireStore
    .collection("posts")
    .orderBy("lastModified", "desc")
    .limit(howMany + 1);
  return ref.get();
};

const getPostDetail = (id) => {
  let ref = fireStore.collection("posts").doc(id);
  return ref.get();
};

const saveErrorRequest = (data) => {
  data.response = data.response || "";
  data.callAt = dayjs(data.callAt).format("DD/MM/YYYY, [at] HH:mm");
  // Add a new document with a generated id.
  let newPostRef = fireStore.collection("errorRequests").doc();
  // later...
  return newPostRef.set(data);
};

export {
  getRandomPosts,
  deletePost,
  createPost,
  updatePost,
  getAllPosts,
  getPostDetail,
  saveErrorRequest,
  updateView,
  createShortLink,
  getShortLink,
  getAllShortLink,
  downloadTaskTrello,
  getAllTrelloTasks,
  deleteTask,
  getSpecialDates
};
