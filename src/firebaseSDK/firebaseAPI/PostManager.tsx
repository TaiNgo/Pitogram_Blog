import * as Utility from "@utility";
import {
  createPost,
  deletePost,
  getAllPosts,
  getPostDetail,
  getRandomPosts,
  saveErrorRequest,
  updatePost,
  updateView,
} from "src/firebaseSDK/fireStore";
import { Post } from "@types";

const CreatePost = (input) =>
  new Promise((resolve, reject) => {
    try {
      //check input
      let validateBody = checkBody(input);
      if (!validateBody.success)
        resolve(Utility.generateErrorResponse(validateBody.message));

      createPost(input)
        .then((data) => {
          resolve(Utility.generateSuccessResponse(data));
        })
        .catch((err) => {
          saveErrorRequest({
            function: "createPost",
            callAt: Date.now(),
            response: err,
            message: err.message,
          });
          resolve(Utility.generateErrorResponse(err));
        });
    } catch (e) {
      saveErrorRequest({
        function: "createPost",
        callAt: Date.now(),
        error: e.toString(),
        message: e.message || "",
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

const checkBody = (data: Post) => {
  let missingData: string[] = [];
  let notRequired = ["views", "password"];

  Object.keys(data).forEach((k) => {
    if (
      notRequired.indexOf(k) == -1 &&
      (!data[k] || (data[k] + "").length == 0)
    ) {
      missingData.push(" " + k);
      return true;
    }
  });

  return Object.keys(data).length == 0 || missingData.length > 0
    ? { success: false, message: "missing data!" }
    : { success: true };
};

const DeletePost = (id) =>
  new Promise((resolve, reject) => {
    deletePost(id)
      .then(() => {
        resolve(Utility.generateSuccessResponse(id));
      })
      .catch((err) => {
        saveErrorRequest({
          function: "deletePost",
          callAt: Date.now(),
          response: err,
          message: err.message,
        });
        resolve(Utility.generateErrorResponse("cannot delete"));
      });
  });

const GetDetail = (id) =>
  new Promise((resolve, reject) => {
    getPostDetail(id)
      .then((doc) => {
        if (doc.exists) {
          resolve(Utility.generateSuccessResponse(doc.data()));
        } else {
          resolve(Utility.generateErrorResponse("No data!"));
        }
      })
      .catch((e) => {
        saveErrorRequest({
          function: "getPostDetail",
          callAt: Date.now(),
          response: e || "",
          message: e.message,
        });
        resolve(Utility.generateErrorResponse("Cannot get posts!"));
      });
  });

const GetPosts = () =>
  new Promise((resolve, reject) => {
    getAllPosts()
      .then((querySnapshot) => {
        let data: any = [];
        querySnapshot.forEach((doc) => {
          data.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        resolve(Utility.generateSuccessResponse(data));
      })
      .catch((e) => {
        saveErrorRequest({
          function: "getAllPosts",
          callAt: Date.now(),
          response: e || "",
          message: e.message,
        });
        resolve(Utility.generateErrorResponse("Cannot get posts!"));
      });
  });

const GetRandomPosts = (howMany) =>
  new Promise((resolve, reject) => {
    getRandomPosts(howMany)
      .then((querySnapshot) => {
        let data: any = [];
        querySnapshot.forEach((doc) => {
          data.push({
            id: doc.id,
            ...doc.data(),
          });
        });
        resolve(Utility.generateSuccessResponse(data));
      })
      .catch((e) => {
        saveErrorRequest({
          function: "getAllPosts",
          callAt: Date.now(),
          response: e || "",
          message: e.message,
        });
        resolve(Utility.generateErrorResponse("Cannot get posts!"));
      });
  });

const IncreaseView = (id) => {
  try {
    updateView(id)
      .then(() => {
        return true;
      })
      .catch((err) => {
        saveErrorRequest({
          function: "updateView",
          callAt: Date.now(),
          error: err,
          message: err.message,
        });
        return false;
      });
  } catch (e) {
    saveErrorRequest({
      function: "updateView",
      callAt: Date.now(),
      error: e,
      message: e.message,
    });
    return false;
  }
};

const UpdatePost = (input) =>
  new Promise((resolve, reject) => {
    try {
      //check input
      let validateBody = checkBody(input);
      if (!validateBody.success)
        return Utility.generateErrorResponse(validateBody.message);

      updatePost(input)
        .then((data) => {
          resolve(Utility.generateSuccessResponse(input));
        })
        .catch((err) => {
          saveErrorRequest({
            function: "createPost",
            callAt: Date.now(),
            response: err,
            message: err.message,
          });
          resolve(Utility.generateErrorResponse(err));
        });
    } catch (e) {
      saveErrorRequest({
        function: "createPost",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

export {
  CreatePost,
  DeletePost,
  GetDetail,
  GetPosts,
  GetRandomPosts,
  IncreaseView,
  UpdatePost,
};
