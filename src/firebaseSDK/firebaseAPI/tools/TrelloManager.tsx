import * as Utility from "@utility";
import {
  saveErrorRequest,
  deleteTask,
  downloadTaskTrello,
  getAllTrelloTasks,
} from "src/firebaseSDK/fireStore";

const DeleteTask = (id) =>
  new Promise((resolve, reject) => {
    try {
      deleteTask(id);
    } catch (e) {
      saveErrorRequest({
        function: "deleteTask",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

const ModifyDownloadTask = (id, fileName, filesCount, status, url) =>
  new Promise((resolve, reject) => {
    try {
      downloadTaskTrello(id, fileName, filesCount, status, url)
        .then(() => {
          resolve(Utility.generateSuccessResponse(id));
        })
        .catch((e) => {
          saveErrorRequest({
            function: "downloadTaskTrello",
            callAt: Date.now(),
            response: e,
            message: e.message,
          });
          resolve(Utility.generateErrorResponse(e));
        });
    } catch (e) {
      saveErrorRequest({
        function: "downloadTaskTrello",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

const GetAllDownloadTask = () =>
  new Promise((resolve, reject) => {
    try {
      getAllTrelloTasks()
        .then((querySnapshot) => {
          let data: any = [];
          querySnapshot.forEach((doc) => {
            data.push({
              id: doc.id,
              ...doc.data(),
            });
          });
          resolve(Utility.generateSuccessResponse(data));
        })
        .catch((e) => {
          saveErrorRequest({
            function: "GetAllDownloadTask",
            callAt: Date.now(),
            response: e,
            message: e.message,
          });
          resolve(Utility.generateErrorResponse(e));
        });
    } catch (e) {
      saveErrorRequest({
        function: "GetAllDownloadTask",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

export { DeleteTask, ModifyDownloadTask, GetAllDownloadTask };
