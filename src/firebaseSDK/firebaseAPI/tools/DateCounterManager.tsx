import * as Utility from "@utility";
import { saveErrorRequest, getSpecialDates } from "src/firebaseSDK/fireStore";

const GetSpecialDates = () =>
  new Promise((resolve, reject) => {
    try {
      getSpecialDates()
        .then((querySnapshot) => {
          let data: any = [];
          querySnapshot.forEach((doc) => {
            data.push({
              id: doc.id,
              ...doc.data(),
            });
          });
          resolve(Utility.generateSuccessResponse(data));
        })
        .catch((e) => {
          saveErrorRequest({
            function: "getSpecialDates",
            callAt: Date.now(),
            response: e,
            message: e.message,
          });
          resolve(Utility.generateErrorResponse(e));
        });
    } catch (e) {
      saveErrorRequest({
        function: "getSpecialDates",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

export { GetSpecialDates };
