import * as Utility from "@utility";
import {
  saveErrorRequest,
  createShortLink,
  getAllShortLink,
  getShortLink,
} from "src/firebaseSDK/fireStore";

const CreateShortLink = (url, id) =>
  new Promise((resolve, reject) => {
    try {
      GetLink(id).then((res: any) => {
        if (!res["data"]) {
          createShortLink(url, id)
            .then(() => {
              resolve(
                Utility.generateSuccessResponse({
                  shortedLink: `${process.env.NEXT_PUBLIC_DOMAIN}/l/${id}`,
                  source: url,
                })
              );
            })
            .catch((e) => {
              saveErrorRequest({
                function: "createShortLink",
                callAt: Date.now(),
                response: e,
                message: e.message,
              });
              resolve(Utility.generateErrorResponse(e));
            });
        } else {
          resolve(
            Utility.generateSuccessResponse({
              shortedLink: `${process.env.NEXT_PUBLIC_DOMAIN}/l/${id}`,
              source: url,
            })
          );
        }
      });
    } catch (e) {
      saveErrorRequest({
        function: "createShortLink",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

const GetAllLink = () =>
  new Promise((resolve, reject) => {
    try {
      getAllShortLink()
        .then((querySnapshot) => {
          let data: any = [];
          querySnapshot.forEach((doc) => {
            data.push({
              id: doc.id,
              ...doc.data(),
            });
          });
          resolve(Utility.generateSuccessResponse(data));
        })
        .catch((e) => {
          saveErrorRequest({
            function: "getShortLink",
            callAt: Date.now(),
            response: e,
            message: e.message,
          });
          resolve(Utility.generateErrorResponse(e));
        });
    } catch (e) {
      saveErrorRequest({
        function: "getShortLink",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

const GetLink = (input) =>
  new Promise((resolve, reject) => {
    try {
      getShortLink(input)
        .then((doc) => {
          resolve(Utility.generateSuccessResponse(doc.data()));
        })
        .catch((e) => {
          saveErrorRequest({
            function: "getShortLink",
            callAt: Date.now(),
            response: e,
            message: e.message,
          });
          resolve(Utility.generateErrorResponse(e));
        });
    } catch (e) {
      saveErrorRequest({
        function: "getShortLink",
        callAt: Date.now(),
        response: e,
        message: e.message,
      });
      resolve(Utility.generateErrorResponse(e));
    }
  });

export { CreateShortLink, GetAllLink, GetLink };
