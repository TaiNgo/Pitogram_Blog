import firebase from 'firebase';
import 'firebase/storage';   // <-- See the addition here

var firebaseConfig = {
   apiKey: "AIzaSyD28kTX7kH2rOWWsjN95ht3EIq9CVhVET0",
   authDomain: "taingoblog.firebaseapp.com",
   databaseURL: "https://taingoblog-default-rtdb.asia-southeast1.firebasedatabase.app",
   projectId: "taingoblog",
   storageBucket: "taingoblog.appspot.com",
   messagingSenderId: "514010881073",
   appId: "1:514010881073:web:6235e9082877d335e8c910",
   measurementId: "G-DK33C13DR0"
 };

  // Initialize Firebase
  if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
 } else {
    firebase.app(); // if already initialized, use that one
 }

const fireStore =  firebase.firestore();
const fireStorage =  firebase.storage();

export { firebase, fireStore, fireStorage }