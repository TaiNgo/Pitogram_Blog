import { fireStorage } from "./firebase";

const upload = ({ meta, file }, callback) => {
  const storageRef = fireStorage.ref();

  // Upload file and metadata to the object 'images/mountains.jpg'
  let uploadTask = storageRef.child("thumbnails/" + meta.name).put(file, meta);

  uploadTask.then((data) => {
    data.ref.getDownloadURL().then((url) => {
      // do whatever you want with url
      callback(url);
    });
  });
};

const uploadFromBase64 = ({ meta, file }, callback) => {
  const storageRef = fireStorage.ref();
  let uploadTask = storageRef
    .child("trello/" + meta.name)
    .put(file);

  uploadTask.then((data) => {
    data.ref.getDownloadURL().then((url) => {
      // do whatever you want with url
      callback(url);
    });
  });
};

export { upload, uploadFromBase64 };
