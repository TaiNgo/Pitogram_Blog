
 class Post {
  id: string
  title: string
  content: string
  authorID: string
  created: number
  lastModified: number
  thumbnailUrl: string
  views: number
  password: string
  track: string
}

class CreatePostParam {
  title: string
  content: string
  authorID: string
  created: number
  lastModified: number
  thumbnailUrl: string
  views: number
  password: string
}

class EditPostParam {
  id: string
  title: string
  content: string
  lastModified: number
  thumbnailUrl: string
  password: string
}

class Request {
  method: string
  url: string
  function: string
  callAt: any
  message: any
  response: any
}

export { Post, Request, EditPostParam, CreatePostParam }