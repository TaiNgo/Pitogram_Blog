import * as Utility from "@utility";
const del = require("del");

export default async function handler(req, res) {
  try {
    try {
      await del(`./downloaded`);
      console.log(`downloaded is deleted!`);
    } catch (err) {
      console.error(`Error while deleting downloaded.`);
    }
    res.status(200).json(Utility.generateSuccessResponse("Done!"))
  } catch (err) {
    res.status(200).json(Utility.generateErrorResponse(err));
  }
}
