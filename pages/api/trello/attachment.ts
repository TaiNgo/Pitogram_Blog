import * as Trello from "node-trello";
const APIKey = "2efe513b6ca7079fbfc916dd7a3e9a11";

import * as Utility from "@utility";

export default function handler(req, res) {
  const {
    query: { userToken, cardId },
  } = req;

  try {
    getBoards(userToken, cardId).then((data) => {
      res.status(200).json(data)
    })
  } catch (err) {
    res.status(200).json(Utility.generateErrorResponse(err));
  }
}

const getBoards = (userToken, cardId) => {
  let trello = new Trello(APIKey, userToken);
  return new Promise((resolve, reject) => {
    trello.get(`/1/cards/${cardId}/attachments/`, function (err, data) {
      if (err) resolve(Utility.generateErrorResponse(err));
      else resolve(Utility.generateSuccessResponse(data));
    });
  });
};
