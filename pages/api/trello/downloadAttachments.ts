import * as Utility from "@utility";
import { existsSync, mkdirSync, createWriteStream, readFileSync } from "fs";
import axios from "axios";
import { uploadFromBase64 } from "src/firebaseSDK/fireStorage";
import { DeleteTask, ModifyDownloadTask } from "@firebaseSDK/firebaseAPI/tools/TrelloManager";
var zipdir = require("zip-dir");
const del = require("del");
global.XMLHttpRequest = require("xhr2");

export default async function handler(req, res) {
  const {
    body: { imgData, downloadTaskId },
  } = req;

  if (imgData.length == 0) {
    res.status(200).json(Utility.generateErrorResponse("No attachments!"));
  } else {
    try {
      handlingFile(req);

      res.status(200).json(Utility.generateSuccessResponse(downloadTaskId));
    } catch (err) {
      res.status(200).json(Utility.generateErrorResponse(err));
    }
  }
}

const handlingFile = async (req) => {
  const {
    body: { folderName, imgData, downloadTaskId },
    headers: { apikey, usertoken },
  } = req;

  let count = imgData.map((v) => v[Object.keys(v)[0]]).flat().length;
  console.log(`=========Will download ${count} files!=============`);

  for (let k = 0; k < imgData.length; k++) {
    const e = imgData[k];
    const cardName = Object.keys(e)[0];
    console.log(
      `=========Downloading ${e[cardName].length} files in ${cardName}!=============`
    );
    const path = `./downloaded/${folderName}/${k+1}. ${cardName}/`;
    let config = {
      path,
      headers: {
        Authorization:
          'OAuth oauth_consumer_key="' +
          apikey +
          '", oauth_token="' +
          usertoken +
          '"',
      },
      responseType: "stream",
    };
    try {
      await Promise.all(
        e[cardName].map((url, j) => {
          return downloadFile(url, {
            ...config,
            fileName: j + 1 + "." + url.split(".").pop(),
          });
        })
      );
    } catch (e) {
      console.log(e)
      DeleteTask(downloadTaskId);
    }
  }

  console.log("=========Zipping!=============");
  try {
    let file = await zipdir(`./downloaded/${folderName}`);

    try {
      await del(`./downloaded/${folderName}`);
      console.log(`${folderName} is deleted!`);
    } catch (err) {
      console.error(`Error while deleting ${folderName}.`);
    }

    console.log("=========Uploading to Firebase!=============");
    uploadFromBase64({ meta: { name: folderName + ".zip" }, file }, (url) => {
      //update download task status and url
      ModifyDownloadTask(downloadTaskId, folderName, count, 1, url);
      console.log("=========Uploaded!=============");
    });
  } catch (err) {
    console.log("Zip error!");
    console.log(err);
  }
};

const downloadFile = (url, config) => {
  return new Promise((resolve, reject) => {
    axios
      .get(url, { ...config })
      .then((res) => {
        if (res["status"] == 200) {
          let { path, fileName } = config;
          if (!existsSync(path)) {
            mkdirSync(path, { recursive: true });
          }

          const writer = createWriteStream(path + fileName);
          res.data.pipe(writer);
          writer.on("finish", resolve);
          writer.on("error", reject);
        } else {
          reject("Something went wrong!");
        }
      })
      .catch((err) => {
        console.log(err.message);
        reject("This board is private, Please login!");
      });
  });
};
