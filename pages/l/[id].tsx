import React, { useEffect } from "react";
import Head from "next/head";
import style from "./index.module.scss";
import { LoadingOutlined } from "@ant-design/icons";
import { Button, Result, Spin } from "antd";
import Router from 'next/router'
import { GetLink, GetAllLink } from "@firebaseSDK/firebaseAPI/tools/ShortLinkManager";

const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />;

const title = "Shorten Link Free";
const content = "";

const ShortenLink = (data) => {
  const { id, source } = data;

  useEffect(() => {
    setTimeout(() => {
      if (source?.length > 0) Router.push(source);
    }, 2000);
  }, [source]);

  return (
    <>
      <Head>
        {/* <!-- Primary Meta Tags --> */}
        <title>{title}</title>
        <meta name="title" content={title} key="title" />
        <meta name="description" content={content} key="description" />

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website" key="fb-type" />
        <meta
          property="og:url"
          content={`https://pitogram.com/l/${id}`}
          key="fb-url"
        />
        <meta property="og:title" content={title} key="fb-title" />
        <meta
          property="og:description"
          content={content}
          key="fb-description"
        />
        <meta
          property="og:image"
          content="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/shorten-thumb.png?alt=media&token=8b09de43-7083-4065-a200-e5995f19867e"
          key="fb-img"
        />
      </Head>
      <div className={`${style.container}`}>
        {!source || source.length == 0 ? (
          <>
            <Result
              status="404"
              title="404"
              subTitle="Sorry, the page you visited does not exist."
            />
          </>
        ) : (
          <>
            {/* <Spin indicator={antIcon} /> */}
            <Button size="large" onClick={() => {
              Router.push(source)
            }}>Click to continue</Button>
          </>
        )}
      </div>
    </>
  );
};
export default ShortenLink;

// export const getServerSideProps = async ({ params }) => {
//   try {
//     let id = params.id;
//     let res: any = await GetLink(id);
//     if (res["success"])
//       return {
//         props: { ...res["data"], id }
//       };
//     else
//       return {
//         props: {
//           id: params.id,
//         }
//       };
//   } catch (e) {
//     return {
//       props: {
//         id: params.id,
//       }
//     };
//   }
// };

export const getStaticProps = async ({ params }) => {
  try {
    let id = params.id;
    let res: any = await GetLink(id);
    if (res["success"])
      return {
        props: { ...res["data"], id },
        revalidate: 1,
      };
    else
      return {
        props: {
          id: params.id,
        },
        revalidate: 1,
      };
  } catch (e) {
    return {
      props: {
        id: params.id,
      },
      revalidate: 1,
    };
  }
};

export const getStaticPaths = async () => {
  try {
    let res: any = await GetAllLink();
    const paths = res["data"].map((v: any) => {
      return {
        params: {
          id: `${v.id}`,
        },
      };
    });

    return {
      paths,
      // fallback: false // bat ki path nao k returned boi getStaticPaths se toi trang 404
      fallback: true, // path nao k returned ngay lap tuc se show trang "tam thoi" => doi getStaticProps chay
      // => getStaticProps chay xong => return trang hoan chinh
    };
  } catch (e) {
    return {
      paths: [],
      // fallback: false // bat ki path nao k returned boi getStaticPaths se toi trang 404
      fallback: true, // path nao k returned ngay lap tuc se show trang "tam thoi" => doi getStaticProps chay
      // => getStaticProps chay xong => return trang hoan chinh
    };
  }
};