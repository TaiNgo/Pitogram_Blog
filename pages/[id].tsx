import React, { useEffect, useState } from "react";
import Head from "next/head";
import { Post } from "@types";
import style from "./index.module.scss";
import { Avatar, Button, Input, message } from "antd";
import { IncreaseView, GetPosts, GetDetail } from "@firebaseSDK/firebaseAPI/PostManager";
import Link from "next/link";

//dayjs
import dayjs from "dayjs";
import "dayjs/locale/en";
dayjs.locale("en");

//Remember to import the dynamic from Nextjs
import dynamic from "next/dynamic";

let Editor;
if (typeof window !== "undefined") {
  Editor = dynamic(() => import("@components/Editor"), { ssr: false });
}

const parseTime = (time) => {
  return dayjs(time).format("DD/MM/YYYY, [at] HH:mm");
};

const Article = ({ data, morePosts = [] }) => {
  let {
    id,
    title,
    created,
    lastModified,
    thumbnailUrl,
    content,
    views,
    password: postPassword,
    track,
  }: Post = data || {};

  const [isProtected, setIsprotected] = useState(postPassword?.length > 0);
  const [password, setPassword] = useState("");

  useEffect(() => {
    IncreaseView(id);
  }, []);

  const error = (text) => {
    message.error(text);
  };

  const header = (
    <Head>
      {/* <!-- Primary Meta Tags --> */}
      <title>{title}</title>
      <meta name="title" content={title} key="title" />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" key="fb-type" />
      <meta
        property="og:url"
        content={`https://pitogram.com/${id}`}
        key="fb-url"
      />
      <meta property="og:title" content={title} key="fb-title" />
      <meta property="og:image" content={thumbnailUrl} key="fb-img" />
    </Head>
  );

  const renderSpotifyTrack = () => {
    return (
      <div className={`${style.track}`}>
        <iframe
          style={{ borderRadius: "12px" }}
          src={track}
          width="100%"
          height="80"
          frameBorder="0"
          allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
        ></iframe>
      </div>
    );
  };

  const renderMorePosts = () => {
    return (
      <div className={`${style.list}`}>
        {morePosts?.map((p: Post) => (
          <Link href={`/${p.id}`} key={p.id}>
            <a className={`${style.card}`} style={{ color: "black" }}>
              <img src={p.thumbnailUrl} className={`${style.image}`} />
              <div className={`${style.info}`}>
                <p className={style.title}>{p.title}</p>
                <div className={style.card_author}>
                  <Avatar
                    size={26}
                    src="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/0019_17A.jpg?alt=media&token=49024a1b-23d2-48f2-b18d-8ba61ca1c1e2"
                  />
                  <div className={`${style.name_time}`}>
                    <p className={style.name}>{p.authorID}</p>
                    <p className={style.time}>{parseTime(p.created)}</p>
                  </div>
                </div>
              </div>
            </a>
          </Link>
        ))}
      </div>
    );
  };

  return (
    <>
      {header}
      {isProtected ? (
        <>
          <div className={`${style.container}`}>
            <div style={{ margin: "auto", marginTop: "3em" }}>
              <h4>Password</h4>
              <Input.Password
                onPressEnter={() =>
                  password == postPassword
                    ? setIsprotected(false)
                    : error("Incorrect password!")
                }
                onChange={(e) => setPassword(e.target.value)}
              />
              <Button
                onClick={() =>
                  password == postPassword
                    ? setIsprotected(false)
                    : error("Incorrect password!")
                }
                type="primary"
                size="middle"
                style={{ marginTop: "0.5em", display: "block", width: "100%" }}
              >
                Open
              </Button>
            </div>
          </div>
        </>
      ) : (
        <>
          <div className={`${style.container}`}>
            <h1 className={`${style.title}`}>{title}</h1>
            <div className={`${style.author}`}>
              <Avatar
                size={40}
                src={
                  "https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/0019_17A.jpg?alt=media&token=49024a1b-23d2-48f2-b18d-8ba61ca1c1e2"
                }
              />
              <div className={`${style.info}`}>
                <p className={style.name}>Tai Ngo</p>
                <p className={style.time}>{parseTime(created)}</p>
              </div>
            </div>
            <img className={`${style.thumb}`} src={thumbnailUrl} />
            {track && track.length > 0 && renderSpotifyTrack()}
            <div className={`${style.content}`}>
              {Editor && <Editor data={content} key={id} readOnly={true} />}
            </div>
            <div className={`${style.footer}`}>
              <p className={`${style.lastModify}`}>
                Last edited {parseTime(lastModified)}
              </p>
              <p className={`${style.views}`}>{views} views</p>
              <div className={`${style.relateArticle}`}>
                {/* <p className={`${style.relateArticleTitle}`}>Other posts</p> */}
                {/* {renderMorePosts()} */}
              </div>
            </div>
          </div>
        </>
      )}
    </>
  );
};
export default Article;

export const getStaticProps = async ({ params }) => {
  try {
    let id = params.id;
    let res: any = await GetDetail(id);
    if (res["success"])
      return {
        props: {
          data: { ...res["data"], id },
        },
        revalidate: 10,
      };
    else
      return {
        props: {
          id: params.id,
        },
        revalidate: 10,
      };
  } catch (e) {
    return {
      props: {
        id: params.id,
      },
      revalidate: 10,
    };
  }
};

export const getStaticPaths = async () => {
  try {
    let res: any = await GetPosts();
    const paths = res["data"].map((v: Post) => {
      return {
        params: {
          id: `${v.id}`,
        },
      };
    });

    return {
      paths,
      // fallback: false // bat ki path nao k returned boi getStaticPaths se toi trang 404
      fallback: true, // path nao k returned ngay lap tuc se show trang "tam thoi" => doi getStaticProps chay
      // => getStaticProps chay xong => return trang hoan chinh
    };
  } catch (e) {
    return {
      paths: [],
      // fallback: false // bat ki path nao k returned boi getStaticPaths se toi trang 404
      fallback: true, // path nao k returned ngay lap tuc se show trang "tam thoi" => doi getStaticProps chay
      // => getStaticProps chay xong => return trang hoan chinh
    };
  }
};
