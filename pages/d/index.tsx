import React, { useEffect, useState } from "react";
import Head from "next/head";
import style from "./index.module.scss";

//dayjs
import dayjs from "dayjs";
import "dayjs/locale/en";
dayjs.locale("en");

const countDays = (dateString) => {
  let years, months, days, raw;
  let isValid = verify(dateString);
  if (!isValid) return { years, months, days, raw, isValid };

  let d1 = parseDay(dateString);
  let d2 = dayjs();

  raw = d1.diff(d2);
  years = Math.abs(d1.diff(d2, "y"));
  d1 = d1.add(-d1.diff(d2, "y"), "year");

  months = Math.abs(d1.diff(d2, "M"));
  d1 = d1.add(-d1.diff(d2, "M"), "M");

  days = Math.abs(d1.diff(d2, "d"));

  return { years, months, days, raw, isValid };
};

const parseDay = (dateString) => {
  let d = dateString.substring(0, 2);
  let m = dateString.substring(2, 4);
  let y = dateString.substring(4, 8);

  let d1 = dayjs(`${y}-${m}-${d}`);

  return d1;
};

const verify = (dateString) => {
  let d = dateString.substring(0, 2) || undefined;
  let m = dateString.substring(2, 4) || undefined;
  let y = dateString.substring(4, 8) || undefined;
  return (
    d &&
    d <= 31 &&
    m &&
    m <= 12 &&
    y &&
    dayjs(`${y}-${m}-${d}`, "YYYY-MM-DD", true).isValid()
  );
};

const resolveSpecialDates = (dateString) => {
  let specialDates = {
    noel: {
      date: "2412" + dayjs().year(),
      title: "Noel " + dayjs().year(),
    },
    tet2022: {
      date: "01022022",
      title: "Tết 2022",
    },
  };
  return specialDates[dateString] || { date: dateString };
};

const DayCount = () => {
  const description = "01/12/2020 -> pitogram.com/d/01122020";
  const thumbnailUrl = "https://media.graphcms.com/r8eWSl1dR6RrTHaA7DWn";

  const getCountString = (date) => {
    const dateObject = resolveSpecialDates(date);
    const dateTitle = dateObject.title;
    date = dateObject.date;
    const countObject = countDays(date);

    let countString = "";

    if (countObject.isValid) {
      if (countObject.years > 0) {
        countString = countString + countObject.years + " năm ";
      }
      if (countObject.months > 0) {
        countString = countString + countObject.months + " tháng ";
      }
      if (countObject.days > 0) {
        countString = countString + countObject.days + " ngày";
      }

      if (countObject.raw < 0) {
        countString =
          "Đã " + countString + " từ " + dateTitle ||
          parseDay(date).format("DD/MM/YYYY");
      } else {
        countString =
          "Còn " + countString + " nữa đến " + dateTitle ||
          parseDay(date).format("DD/MM/YYYY");
      }

      if (
        countObject.years == 0 &&
        countObject.months == 0 &&
        countObject.days == 0
      ) {
        countString = "Mới đây mà";
      }
    } else {
      countString = "Không hợp lệ";
    }

    return countString;
  };

  const header = (
    <Head>
      {/* <!-- Primary Meta Tags --> */}
      <title>Date Counter</title>
      <meta name="title" content="Date Counter" key="title" />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" key="fb-type" />
      <meta
        property="og:url"
        content={`https://pitogram.com/d`}
        key="fb-url"
      />
      <meta property="og:title" content="Date Counter" key="fb-title" />
      <meta
        property="og:description"
        content={description}
        key="fb-description"
      />
      <meta property="og:image" content={thumbnailUrl} key="fb-img" />
    </Head>
  );

  return (
    <>
      {header}  
      <div className={`${style.container}`}>
        <div style={{ margin: "auto", marginTop: "3em" }} className={`${style.content}`}>
          <h1>Welcome</h1>
          <p>This tool is used for sending date counting in Messenger.</p>
          <p>Try to send this message to your friend: <b>"blog.pitogram.com/d/0105202318"</b></p>
          <p>The url format would be <b>"blog.pitogram.com/d/ddmmyyyhh"</b></p>
          <p>The result would be: </p>
          <img src="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/thumbnails%2F2022-01-26%2020_22_41-Facebook.png?alt=media&token=9b8909f3-b55c-4ef1-a700-cf895b2d5cc5"/>
          <br></br>
          <br></br>
        </div>
      </div>
    </>
  );
};

export default DayCount;
