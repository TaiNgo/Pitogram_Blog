import React, { useEffect, useState } from "react";
import Head from "next/head";
import style from "./index.module.scss";

//dayjs
import dayjs from "dayjs";
import "dayjs/locale/en";
import { GetSpecialDates } from "@firebaseSDK/firebaseAPI/tools/DateCounterManager";
dayjs.locale("en");

import utc from 'dayjs/plugin/utc'
import timezone from 'dayjs/plugin/timezone'
dayjs.extend(utc)
dayjs.extend(timezone)

dayjs.tz.setDefault("Asia/Bangkok")

const DayCount = ({ date, specialDates }) => {
  const description = "18h 01/12/2022 -> pitogram.com/d/0112202218";
  const thumbnailUrl = "https://media.graphcms.com/r8eWSl1dR6RrTHaA7DWn";
  const [countDayString, setCountDayString] = useState("");

  useEffect(() => {
    setCountDayString(getcountString(date));
    let interval = setInterval(() => {
      GetSpecialDates().then((res: any) => {
        specialDates = res["data"] || [];
      });

      setCountDayString(getcountString(date));
    }, 2000);

    return () => clearInterval(interval);
  }, []);

  const countDays = (dateString) => {
    let years, months, days, hours, mins, raw;
    let isValid = verify(dateString);
    if (!isValid) return { years, months, days, raw, isValid };

    let date = parseDay(dateString);
    let now = dayjs(); //now

    raw = date.diff(now); //miliseconds

    years = Math.abs(date.diff(now, "y"));
    date = date.add(-date.diff(now, "y"), "year");

    months = Math.abs(date.diff(now, "M"));
    date = date.add(-date.diff(now, "M"), "M");

    days = Math.abs(date.diff(now, "d"));
    date = date.add(-date.diff(now, "d"), "d");

    hours = Math.abs(date.diff(now, "h"));
    date = date.add(-date.diff(now, "h"), "h");

    mins = Math.abs(date.diff(now, "m"));

    return { years, months, days, hours, mins, raw, isValid };
  };

  const parseDay = (dateString) => {
    let d = dateString.substring(0, 2);
    let m = dateString.substring(2, 4);
    let y = dateString.substring(4, 8);
    let h = dateString.substring(8, 10);

    let d1 = dayjs(`${y}-${m}-${d} ${h}:00`);
    console.log(d1)

    return d1;
  };

  const verify = (dateString) => {
    try {
      let d = dateString.substring(0, 2) || undefined;
      let m = dateString.substring(2, 4) || undefined;
      let y = dateString.substring(4, 8) || undefined;
      let h = dateString.substring(8, 10) || undefined;

      return (
        d &&
        d <= 31 &&
        m &&
        m <= 12 &&
        y &&
        h &&
        h < 25 &&
        h >= 0 &&
        dayjs(`${y}-${m}-${d}`, "YYYY-MM-DD", true).isValid()
      );
    } catch (e) {
      return false;
    }
  };

  const resolveSpecialDates = (dateString) => {
    let result: any = specialDates.find((d: any) => d.id == dateString) || {
      date: dateString,
    };
    return result;
  };

  const getcountString = (date) => {
    const dateObject = resolveSpecialDates(date);
    const dateTitle = dateObject.title || null;
    date = dateObject.date;

    const countObject = countDays(date);

    let countString = "";

    if (countObject.isValid) {
      if (countObject.years > 0) {
        countString = countString + countObject.years + " năm ";
      }
      if (countObject.months > 0) {
        countString = countString + countObject.months + " tháng ";
      }
      if (countObject.days > 0) {
        countString = countString + countObject.days + " ngày ";
      }
      if (countObject.hours > 0) {
        countString = countString + countObject.hours + " giờ ";
      }
      if (countObject.hours > 0) {
        countString = countString + countObject.mins + " phút";
      }

      if (countObject.raw < 0) {
        countString =
          "Đã " +
          countString +
          " từ " +
          (dateTitle || parseDay(date).format("HH:mm DD/MM/YYYY"));
      } else {
        countString =
          "Còn " +
          countString +
          " nữa đến " +
          (dateTitle || parseDay(date).format("HH:mm DD/MM/YYYY"));
      }

      if (
        countObject.years == 0 &&
        countObject.months == 0 &&
        countObject.days == 0 &&
        countObject.hours == 0 &&
        countObject.mins == 0
      ) {
        countString = "Mới đây mà";
      }
    } else {
      countString = "Không hợp lệ";
    }

    return countString;
  };

  const header = (
    <Head>
      {/* <!-- Primary Meta Tags --> */}
      <title>{getcountString(date)}</title>
      <meta name="title" content={getcountString(date)} key="title" />

      {/* <!-- Open Graph / Facebook --> */}
      <meta property="og:type" content="website" key="fb-type" />
      <meta
        property="og:url"
        content={`https://pitogram.com/d/${date}`}
        key="fb-url"
      />
      <meta property="og:title" content={getcountString(date)} key="fb-title" />
      <meta
        property="og:description"
        content={description}
        key="fb-description"
      />
      <meta property="og:image" content={thumbnailUrl} key="fb-img" />
    </Head>
  );

  return (
    <>
      {header}
      <div className={`${style.container}`}>
        <div style={{ margin: "auto", marginTop: "3em" }}>
          <h1>{countDayString}</h1>
        </div>
      </div>
    </>
  );
};

export default DayCount;

// This gets called on every request
export async function getServerSideProps({ params }) {
  // Pass data to the page via props
  let res: any = await GetSpecialDates();
  let specialDates = res["data"] || [];
  return { props: { date: params.date, specialDates } };
}
