import React from "react";
import { AppProps } from "next/app";
import "antd/dist/antd.css";
import "@styles/global.scss";
import { Provider } from "react-redux";
import store from "@redux/store";
import Head from "next/head";
import { Header, Footer } from "@components";
import Router, { useRouter } from "next/router";

import NProgress from "nprogress";
import "react-quill/dist/quill.snow.css";
import { BackTop } from "antd";

Router.events.on("routeChangeStart", () => {
  NProgress.start();
});
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ Component, pageProps }): JSX.Element {

  const isAdminPage = () => {
    const router = useRouter();
    return router.pathname.indexOf("admin") !== -1;
  };

  return (
    <Provider store={store}>
      <Head>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Rubik:ital,wght@0,300;0,400;0,500;0,600;1,300;1,400;1,500&display=swap"
          rel="stylesheet"
        />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" />
        <link
          href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,500;1,400;1,500&display=swap"
          rel="stylesheet"
        />

        <link rel="shortcut icon" href="/icons/favicon.ico" />
        {/* <!-- Primary Meta Tags --> */}
        <meta name="title" content="Pitogram blog" key="title" />
        <meta name="description" content="" key="description" />

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website" key="fb-type" />
        <meta property="og:url" content="https://pitogram.com/" key="fb-url" />
        <meta property="og:title" content="Pitogram blog" key="fb-title" />
        <meta property="og:description" content="" key="fb-description" />
        <meta
          property="og:image"
          content="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/pitogram.png?alt=media&token=519ff4e0-d008-4d83-a29f-4e4d3fb4593f"
          key="fb-img"
        />
      </Head>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          minHeight: "100vh",
        }}
      >
        {!isAdminPage() && <Header />}
        <BackTop />
        <Component {...pageProps} />
        {!isAdminPage() && <Footer />}
      </div>
    </Provider>
  );
}

export default MyApp;