import React, { useEffect } from "react";
import { Main } from "@components";
import { setPosts } from "@redux/actions";
import { useAppDispatch } from "@redux/store";
import { GetPosts } from "@firebaseSDK/firebaseAPI/PostManager";

const Home = ({ data }) => {
  //redux
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (data) {
      dispatch(setPosts(data || []));
    }
  }, [data]);

  return (
    <>
      <Main />
    </>
  );
};

export const getStaticProps = async ({ params }) => {
  try {
    let res:any = await GetPosts();
    return {
      props: {
        data: res['data'] || [],
      },
      revalidate: 10,
    };
  } catch (e) {
    return {
      props: {
        data: [],
      },
      revalidate: 10,
    };
  }
};

export default Home;
