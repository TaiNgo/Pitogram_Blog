import React from "react";
import Head from "next/head";
import style from "./index.module.scss";
import { toolList } from "../../src/shared/toolListData";

const Application = ({ id }) => {
  return (
    <>
      <Head>
        {/* <!-- Primary Meta Tags --> */}
        <title>{toolList[id]["title"]}</title>
        <meta name="title" content={toolList[id]["title"]} key="title" />
        <meta
          name="description"
          content={toolList[id]["description"]}
          key="description"
        />

        {/* <!-- Open Graph / Facebook --> */}
        <meta property="og:type" content="website" key="fb-type" />
        <meta
          property="og:url"
          content={`https://pitogram.com/tools/${id}`}
          key="fb-url"
        />
        <meta
          property="og:title"
          content={toolList[id]["title"]}
          key="fb-title"
        />
        <meta
          property="og:description"
          content={toolList[id]["description"]}
          key="fb-description"
        />
        <meta
          property="og:image"
          content="https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/tool-banner.png?alt=media&token=5de229ac-34f2-44be-994a-854998fb7cd8"
          key="fb-img"
        />
      </Head>
      <div className={style.tool}>
        <div className={style.container}>
          <div className={style.title}>
            {/* tim icon o https://icons.getbootstrap.com/ */}
            {toolList[id]["icon"]}
            <h1>{toolList[id]["title"]}</h1>
          </div>
          {/* code tool o day */}
          {toolList[id]["component"]}
        </div>
      </div>
    </>
  );
};
export default Application;

export const getStaticProps = async ({ params }) => {
  return {
    props: {
      id: params.id,
    },
    revalidate: 10,
  };
};

export const getStaticPaths = () => {
  const paths = Object.keys(toolList).map((appName) => {
    return {
      params: {
        id: appName,
      },
    };
  });

  return {
    paths,
    // fallback: false // bat ki path nao k returned boi getStaticPaths se toi trang 404
    fallback: false, // path nao k returned ngay lap tuc se show trang "tam thoi" => doi getStaticProps chay
    // => getStaticProps chay xong => return trang hoan chinh
  };
};
