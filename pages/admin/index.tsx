import React, { useState, useEffect } from "react";
import { Layout, Menu, Breadcrumb, Button, Checkbox, Form, Input } from "antd";
import dynamic from "next/dynamic";
import { useRouter } from "next/router";
import style from "./index.module.scss";

const { Header, Content, Sider } = Layout;

function Index() {
  const router = useRouter();
  const [selected, setSelected] = useState<any>(
    router.query.selected || "dashboard"
  );

  const [loginInfo, setLoginInfo] = useState({
    username: "",
    password: "",
  });

  const onFinish = (values: any) => {
    let { username, password, remember } = values;
    setLoginInfo({
      username,
      password,
    });
    if (remember) {
      window.localStorage.setItem("username", username);
      window.localStorage.setItem("password", password);
    } else {
      window.localStorage.clear();
    }
  };

  const onFinishFailed = (errorInfo: any) => {
    console.log("Failed:", errorInfo);
  };

  const loginForm = (
    <Form
      name="basic"
      labelCol={{ span: 8 }}
      wrapperCol={{ span: 16 }}
      initialValues={{ remember: true }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
    >
      <Form.Item
        label="Username"
        name="username"
        rules={[{ required: true, message: "Please input your username!" }]}
      >
        <Input />
      </Form.Item>

      <Form.Item
        label="Password"
        name="password"
        rules={[{ required: true, message: "Please input your password!" }]}
      >
        <Input.Password />
      </Form.Item>

      <Form.Item
        name="remember"
        valuePropName="checked"
        wrapperCol={{ offset: 8, span: 16 }}
      >
        <Checkbox>Remember me</Checkbox>
      </Form.Item>

      <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
        <Button type="primary" htmlType="submit">
          Submit
        </Button>
      </Form.Item>
    </Form>
  );

  useEffect(() => {
    setSelected(router.query.selected || "dashboard");
  }, [router]);

  useEffect(() => {
    setLoginInfo({
      username: window.localStorage.getItem("username") || "",
      password: window.localStorage.getItem("password") || "",
    });
  }, []);

  const onItemSelect = (e) => {
    let key = e.key;
    router.push(`/admin?selected=${key}`, undefined, { shallow: true });
  };

  const renderContent = () => {
    switch (selected) {
      case "posts":
        let PostManager = dynamic(() => import("./posts"), {
          loading: () => <p>...</p>,
        });
        return <PostManager />;

      case "create-post":
        let CreatePost = dynamic(() => import("./create-post"), {
          loading: () => <p>...</p>,
        });
        return <CreatePost editData={{}} />;

      default:
        return <p>...</p>;
    }
  };

  const checkLogin = () => {
    return (
      loginInfo.username === "taingo" && loginInfo.password === "taingo6798"
    );
  };

  return (
    <>
      {checkLogin() ? (
        <Layout style={{ minHeight: "100vh", maxHeight: "100vh" }}>
          <Sider
            theme="light"
            style={{
              overflow: "auto",
              height: "100vh",
              position: "fixed",
              left: 0,
            }}
          >
            <div className={style.logo}>
              <p className={style.logo_name}>Pitogram</p>
            </div>
            <Menu
              theme="light"
              defaultSelectedKeys={[selected]}
              mode="inline"
              onSelect={(e) => onItemSelect(e)}
              selectedKeys={[selected]}
            >
              <Menu.Item
                key="dashboard"
                icon={<img src="./icons/dashboard.svg" />}
              >
                Dashboard
              </Menu.Item>
              <Menu.Item key="posts" icon={<img src="./icons/newspaper.svg" />}>
                Posts
              </Menu.Item>
              <Menu.Item
                key="create-post"
                icon={<img src="./icons/create.svg" />}
                title="Create"
              >
                Create new Post
              </Menu.Item>
            </Menu>
          </Sider>
          <Layout className={style.site_layout}>
            <Header
              className={style.site_layout_background}
              style={{ padding: 0 }}
            />
            <Content
              style={{ margin: "0 16px" }}
              className={style.layout_content}
            >
              <div
                className={style.site_layout_background}
                style={{ padding: 24, minHeight: "100%" }}
              >
                {renderContent()}
              </div>
            </Content>
          </Layout>
        </Layout>
      ) : (
        <div className={style.login_form}>{loginForm}</div>
      )}
    </>
  );
}

export default Index;
