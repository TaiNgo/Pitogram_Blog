import React, { useEffect, useMemo, useState } from "react";
import style from "./index.module.scss";
import { Button, Input, Image, Upload, message } from "antd";
import { UploadOutlined } from "@ant-design/icons";
import _ from "lodash";

import { CreatePost, UpdatePost } from "@firebaseSDK/firebaseAPI/PostManager";

//firebase
import { upload } from "src/firebaseSDK/fireStorage";

import { CreatePostParam, EditPostParam, Post } from "@types";
import router from "next/router";

//Remember to import the dynamic from Nextjs
import dynamic from "next/dynamic";

let Editor;
if (typeof window !== "undefined") {
  Editor = dynamic(() => import("@components/Editor"), { ssr: false });
}

const fallBackImg =
  "https://firebasestorage.googleapis.com/v0/b/taingoblog.appspot.com/o/default.jpg?alt=media&token=d016dfc6-6628-4cbf-be51-139cd132f4df";

const defaultContent = {
  time: Date.now(),
  blocks: [
    {
      type: "paragraph",
      data: {
        text: "Write here!",
      },
    },
  ],
  version: "2.12.4",
};

const CreatingPost = ({ editData }) => {
  const [content, setContent] = useState(
    (editData && editData["content"]) || defaultContent
  );
  const [thumbUrl, setThumbUrl] = useState(fallBackImg);
  const [title, setTitle] = useState("");
  const [editingId, setEditingId] = useState("");
  const [publishing, setPublishing] = useState(false);
  const [password, setPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");

  const success = (text) => {
    message.success(text);
  };

  const error = (text) => {
    message.error(text);
  };

  useEffect(() => {
    if (editData) {
      let {
        id,
        content,
        title: editingTitle,
        thumbnailUrl,
        password,
      } = editData;
      setContent(content);
      setThumbUrl(thumbnailUrl);
      setTitle(editingTitle);
      setEditingId(id);
      setPassword(password);
    }
  }, [editData]);

  const checkData = (data) => {
    let missingData: string[] = [];
    let notRequired = ["views", "password"];
    Object.keys(data).forEach((k) => {
      if (
        notRequired.indexOf(k) == -1 &&
        (!data[k] || (data[k] + "").length == 0)
      ) {
        missingData.push(" " + k);
        return true;
      }
    });

    return Object.keys(data).length == 0 || missingData.length > 0
      ? { success: false, message: `Missing data: ${missingData}` }
      : { success: true };
  };

  const updatePost = () => {
    setPublishing(true);
    let data: EditPostParam = {
      id: editingId,
      title,
      content,
      lastModified: Date.now(),
      thumbnailUrl: !!thumbUrl ? thumbUrl : fallBackImg,
      password:
        newPassword == "remove"
          ? ""
          : newPassword?.length > 0
          ? newPassword
          : password,
    };

    let validateData = checkData(data);
    if (!validateData.success) {
      error(validateData.message);
      setPublishing(false);
    } else {
      UpdatePost(data)
        .then((res: any) => {
          if (res["success"]) {
            success("Updated!");
            setPublishing(false);
            router.push("admin?selected=posts");
          } else {
            setPublishing(false);
            error("Something went wrong!");
          }
        })
        .catch((err) => {
          setPublishing(false);
          error("Something went wrong!");
        });
    }
  };

  const publish = () => {
    setPublishing(true);
    let data: CreatePostParam = {
      title,
      content,
      authorID: "Tai Ngo",
      created: Date.now(),
      lastModified: Date.now(),
      thumbnailUrl: !!thumbUrl ? thumbUrl : fallBackImg,
      views: 0,
      password: newPassword,
    };

    let validateData = checkData(data);
    if (!validateData.success) {
      setPublishing(false);
      error(validateData.message);
    } else {
      CreatePost(data)
        .then((res: any) => {
          if (res["success"]) {
            setPublishing(false);
            success("Created!");
            router.push("admin?selected=posts");
          } else {
            setPublishing(false);
            error("Something went wrong!");
          }
        })
        .catch((err) => {
          setPublishing(false);
          error("Something went wrong!");
        });
    }
  };

  const uploadToFireBase = (e) => {
    let meta = {
      contentType: e.file.type,
      name: e.file.name,
    };
    upload(
      {
        meta,
        file: e.file,
      },
      (url) => setThumbUrl(url)
    );
  };

  const uploadProps = {
    name: "file",
    showUploadList: false,
    customRequest: (e) => uploadToFireBase(e),
  };

  const editorMemo = useMemo(
    () =>
      Editor && (
        <Editor
          callBack={(data) => setContent(data)}
          data={content || defaultContent}
        />
      ),
    [content]
  );

  return (
    <div className={style.container}>
      {editorMemo}
      <div className={style.tool_bar}>
        <Input
          className={style.title}
          allowClear
          // @ts-ignore
          placeholder={title?.length > 0 ? title : "title here!"}
          onChange={_.debounce((e) => setTitle(e.target.value), 100)}
        />
        <Input
          className={style.title}
          allowClear
          // @ts-ignore
          placeholder={"password here!"}
          onChange={_.debounce((e) => setNewPassword(e.target.value), 100)}
        />
        <Image
          width={"100%"}
          height={300}
          src={thumbUrl || fallBackImg}
          className={style.thumbnail}
        />

        <Upload {...uploadProps} className={style.upload_button}>
          <Button icon={<UploadOutlined />}></Button>
        </Upload>

        <Button
          type="primary"
          onClick={() =>
            editingId && editingId.length > 0 ? updatePost() : publish()
          }
          style={{ marginTop: "1em" }}
          loading={publishing}
        >
          Publish
        </Button>
        <Button
          type="dashed"
          onClick={() => router.push("admin?selected=posts")}
          style={{ marginTop: "1em" }}
        >
          Cancel
        </Button>
      </div>
    </div>
  );
};

export default CreatingPost;
