import React, { useEffect, useState } from "react";
import style from "./index.module.scss";
import { Input, message, Skeleton, Table, Tag, Tooltip } from "antd";
import { ColumnsType } from "antd/lib/table";
import Popup from "@components/popup/popup";

import { RootState } from "@redux/reducers";
import { useAppDispatch } from "@redux/store";
import { useSelector } from "react-redux";
import { setPosts } from "@redux/actions";

import { GetPosts, DeletePost } from "@firebaseSDK/firebaseAPI/PostManager";

import CreatingPost from "../create-post";

//dayjs
import dayjs from "dayjs";
import "dayjs/locale/en";
dayjs.locale("en");

const parseTime = (time) => {
  return dayjs(time).format("DD/MM/YYYY, [at] HH:mm");
};

const columns = [
  {
    title: "Thumbnail",
    dataIndex: "thumbnailUrl",
    key: "2",
    width: 150,
    render: (v) => (
      <img style={{ height: "60px" }} src={v} className={style.thumbnail_img} />
    ),
  },
  {
    title: "Title",
    dataIndex: "title",
    key: "1",
  },
  {
    title: "Author",
    dataIndex: "authorID",
    key: "3",
    render: (v) => (
      <Tag color="blue" style={{ marginLeft: 8 }}>
        {v}
      </Tag>
    ),
  },
  {
    title: "Created",
    dataIndex: "created",
    key: "4",
    render: (v) => parseTime(v),
  },
  {
    title: "Last Modified",
    dataIndex: "lastModified",
    key: "5",
    render: (v) => parseTime(v),
  },
  {
    title: "Views",
    dataIndex: "views",
    key: "6",
    render: (v) => (
      <Tag color="green" style={{ marginLeft: 8 }}>
        {v}
      </Tag>
    ),
  },
];

const PostManager: React.FC = () => {
  const [selectedRowKeys, setSelectedRowKey] = useState([]);
  const [loading, setLoading] = useState(false);
  const [popupSetting, setPopupSetting] = useState({
    record: null,
    visible: false,
    x: 0,
    y: 0,
    callBack: (action, record) => {},
    hidePopup: () => {},
  });

  const [isEdit, setIsEdit] = useState(false);
  const [editData, setEditData] = useState({});

  const success = (text) => {
    message.success(text);
  };

  const error = (text) => {
    message.error(text);
  };

  //redux
  const dispatch = useAppDispatch();
  const posts = useSelector((state: RootState) => state.posts.list);

  const onSelectChange = (selectedRowKeys) => {
    setSelectedRowKey(selectedRowKeys);
  };

  const rowSelection = {
    selectedRowKeys,
    onChange: onSelectChange,
  };

  const hidePopup = () => {
    setPopupSetting({
      record: null,
      visible: false,
      x: 0,
      y: 0,
      callBack: () => {},
      hidePopup: () => {},
    }); // right button click row
  };

  const rowRightClick = (e, record) => {
    e.preventDefault();
    setPopupSetting({
      record,
      visible: true,
      x: e.clientX,
      y: e.clientY,
      callBack: onActionClick,
      hidePopup,
    });
  };

  const onActionClick = (action, record) => {
    hidePopup();
    let { id } = record;

    switch (action) {
      case "delete":
        DeletePost(id)
          .then((res: any) => {
            if (res["success"]) {
              fetchData();
              success("Deleted!");
            } else {
              error("Something went wrong!");
            }
          })
          .catch(() => {
            error("Something went wrong!");
          });
        break;

      case "edit":
        setEditData(record);
        setIsEdit(true);
        break;

      default:
        break;
    }
  };

  const onSearch = (value) => console.log(value);

  const fetchData = () => {
    setLoading(true);
    try {
      GetPosts().then((res: any) => {
        res["success"] && dispatch(setPosts(res["data"] || []));
        setLoading(false);
      });
    } catch (e) {}
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className={style.main}>
      {isEdit ? (
        <CreatingPost editData={editData} />
      ) : (
        <>
          <Popup {...popupSetting} />
          <div className={style.header}>
            <div className={style.actions}>
              <Input
                placeholder="name, title, author,..."
                suffix={<img src="./icons/mic.svg" />}
                className={style.search}
              />
            </div>
            <p className={style.selected_count}>
              {selectedRowKeys.length > 0
                ? `Selected ${selectedRowKeys.length} items`
                : ""}
            </p>
            <Tooltip title="Refresh data">
              <img
                src="./icons/refresh.svg"
                className={style.refresh_icon}
                onClick={() => fetchData()}
              />
            </Tooltip>
          </div>
          <Skeleton loading={loading} active>
            <Table
              rowKey="id"
              rowSelection={rowSelection}
              className={style.table}
              columns={columns as ColumnsType}
              dataSource={posts}
              scroll={{
                y: "calc(100vh - 350px)",
              }}
              onRow={(record, rowIndex) => {
                return {
                  onContextMenu: (event) => rowRightClick(event, record), // right button click row
                  onClick: () => hidePopup(),
                };
              }}
            />
          </Skeleton>
        </>
      )}
    </div>
  );
};

export default PostManager;
